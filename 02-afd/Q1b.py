from afd import DetFiniteAutomaton as DFA

estados = ['q0', 'q1', 'q2', 'q3', 'q4', 'q5']
estados_finais = ['q3', 'q4', 'q5']

automato = DFA(symbols='ab', states=estados, start='q0', finals=estados_finais)
automato.add_transition('q0', 'a', 'q1')
automato.add_transition('q0', 'b', 'q3')
automato.add_transition('q1', 'a', 'q2')
automato.add_transition('q1', 'b', 'q4')
automato.add_transition('q2', 'a', 'q2')
automato.add_transition('q2', 'b', 'q5')
automato.add_transition('q3', 'a', 'q1')
automato.add_transition('q3', 'b', 'q3')
automato.add_transition('q4', 'a', 'q2')
automato.add_transition('q4', 'b', 'q4')
automato.add_transition('q5', 'a', 'q2')
automato.add_transition('q5', 'b', 'q5')

lista_teste = ['b', 'babab', 'baab', 'aab', 'a', 'ab', 'bab', 'aba']

for palavra in lista_teste:
    accepted = f'ACEITA' if automato.accept(palavra) else 'NÃO ACEITA'
    print(f'{palavra:12} {accepted}')
