from afd import DetFiniteAutomaton as DFA

automato = DFA(symbols='01', states=['q0', 'q1', 'q2', 'q3'], start='q0', finals=['q1'])
automato.add_transition('q0', '0', 'q1')
automato.add_transition('q0', '1', 'q2')
automato.add_transition('q1', '0', 'q1')
automato.add_transition('q1', '1', 'q2')
automato.add_transition('q2', '0', 'q3')
automato.add_transition('q3', '0', 'q1')

lista_teste = ['0', '0100', '100', '1', '1001', '010', '0001']

for palavra in lista_teste:
    accepted = f'ACEITA' if automato.accept(palavra) else 'NÃO ACEITA'
    print(f'{palavra:12} {accepted}')
