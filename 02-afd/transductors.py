from typing import Dict, List, Set
from copy import deepcopy
from afd import DetFiniteAutomaton as DFA

class FiniteTransductor(DFA):          
    output_alphabet: Set[str]
    transductions: Dict

    def __init__(self, states: List[str], symbols: List[str], start: str, finals: List[str], outsymbols: str):
        super(FiniteTransductor, self).__init__('', states, start, finals)
        self.alphabet = set(symbols)
        self.output_alphabet = set(outsymbols)
        
        # todas as transduções são inicializadas para None
        self.transductions = dict()
        for state in self.states:
            self.transductions[state] = None


    def add_transduction(self, state: str, output: str):
        assert (state in self.states)
        assert (output in self.output_alphabet)
        self.transductions[state] = output


    def accept(self, sequence: List[str], verbose = False) -> bool:
        for sym in sequence:
            if sym not in self.alphabet:
                raise ValueError(f'Symbol "{sym}" is not in alphabet')

        current_state = self.initial_state
        try:
            while len(sequence) > 0:
                if verbose: print(f'Sequência: {sequence}')
                symbol = sequence.pop(0)
                if verbose: print(f'{current_state} processa {symbol} -> ', end='')
                current_state = self.process(current_state, symbol)
                if verbose: print(current_state)
        except ValueError as e:
            if verbose: print(e)
            return False

        if current_state in self.final_states:
            return True
        else: return False

    
    # Função principal de processamento, recebe uma sequência de símbolos do
    # alfabeto e retorna a sequência de símbolos do alfabeto de saída gerada
    # a partir desse processamento. 
    def process_sequence(self, symbols_sequence: List[str]) -> List[str]:
        generated_sequence = []
        seq = deepcopy(symbols_sequence)
        assert (self.accept(seq))
        
        current_state = self.initial_state
        while True:
            if self.transductions[current_state] is not None:
                generated_sequence.append(self.transductions[current_state])
            
            if len(symbols_sequence) == 0:
                break

            current_symbol = symbols_sequence.pop(0)
            current_state= self.process(current_state, current_symbol)

        return generated_sequence
