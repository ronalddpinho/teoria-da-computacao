from afd import DetFiniteAutomaton as DFA

automato = DFA(
    symbols='abc', 
    states=['q0', 'q1', 'q2', 'q3'], 
    start='q0', 
    finals=['q0', 'q1', 'q2', 'q3'])

automato.add_transition('q0', 'a', 'q1')
automato.add_transition('q1', 'b', 'q1')
automato.add_transition('q1', 'a', 'q3')
automato.add_transition('q1', 'c', 'q2')
automato.add_transition('q2', 'a', 'q1')
automato.add_transition('q2', 'c', 'q2')
automato.add_transition('q3', 'a', 'q3')
automato.add_transition('q3', 'b', 'q1')
automato.add_transition('q3', 'c', 'q2')


lista_teste = ['', 'aa', 'abacabccca', 'b']

for palavra in lista_teste:
    accepted = f'ACEITA' if automato.accept(palavra) else 'NÃO ACEITA'
    print(f'{palavra:12} {accepted}')

# print(automato.accept('abacabccca', verbose=True))