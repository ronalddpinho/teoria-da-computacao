from afd import DetFiniteAutomaton as DFA

states = ['q0', 'q1', 'q2', 'q3', 'q4']
final_states = ['q1', 'q3', 'q4']

automato = DFA(symbols='abc', states=states, start='q0', finals=final_states)
automato.add_transition('q0', 'a', 'q1')
automato.add_transition('q0', 'b', 'q2')
automato.add_transition('q1', 'a', 'q1')
automato.add_transition('q1', 'b', 'q2')
automato.add_transition('q1', 'c', 'q4')
automato.add_transition('q2', 'b', 'q2')
automato.add_transition('q2', 'a', 'q3')
automato.add_transition('q3', 'c', 'q4')
automato.add_transition('q4', 'c', 'q4')

lista_teste = ['ac', 'bac', 'a', 'aabbac', 'abc', 'bc']

for palavra in lista_teste:
    accepted = f'ACEITA' if automato.accept(palavra) else 'NÃO ACEITA'
    print(f'{palavra:12} {accepted}')
