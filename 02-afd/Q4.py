from typing import List
from transductors import FiniteTransductor

estados = ['q0', 'q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7']

tran = FiniteTransductor(
    states=estados,
    symbols=['25', '50', '100'],
    start='q0',
    finals=['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7'],
    outsymbols='01'
)

# Adiciona as transições
# q0
tran.add_transition('q0',  '25', 'q1')
tran.add_transition('q0',  '50', 'q2')
tran.add_transition('q0', '100', 'q3')
# q1
tran.add_transition('q1',  '25', 'q2')
tran.add_transition('q1',  '50', 'q4')
tran.add_transition('q1', '100', 'q5')
# q2
tran.add_transition('q2',  '25', 'q4')
tran.add_transition('q2',  '50', 'q3')
tran.add_transition('q2', '100', 'q6')
# q3
tran.add_transition('q3',  '25', 'q1')
tran.add_transition('q3',  '50', 'q2')
tran.add_transition('q3', '100', 'q3')
# q4
tran.add_transition('q4',  '25', 'q3')
tran.add_transition('q4',  '50', 'q5')
tran.add_transition('q4', '100', 'q7')
# q5
tran.add_transition('q5',  '25', 'q2')
tran.add_transition('q5',  '50', 'q4')
tran.add_transition('q5', '100', 'q5')
# q6
tran.add_transition('q6',  '25', 'q4')
tran.add_transition('q6',  '50', 'q3')
tran.add_transition('q6', '100', 'q6')
# q7
tran.add_transition('q7',  '25', 'q3')
tran.add_transition('q7',  '50', 'q5')
tran.add_transition('q7', '100', 'q7')

# Adiciona as transduções
tran.add_transduction('q1', '0')
tran.add_transduction('q2', '0')
tran.add_transduction('q3', '1')
tran.add_transduction('q4', '0')
tran.add_transduction('q5', '1')
tran.add_transduction('q6', '1')
tran.add_transduction('q7', '1')


# Função executa aplica o transdutor finito a uma lista de símbolos [25, 50 e 100]
def execute_test_for(sequence: List[str]):
    print('Sequência:', ', '.join(sequence))
    generated = tran.process_sequence(sequence)
    print('Gerada:', ' '.join(generated))


execute_test_for(['100', '25', '25', '25', '25', '100', '50', '50', '100', '100', '25', '50', '25', '50', '25', '25', '100'])
