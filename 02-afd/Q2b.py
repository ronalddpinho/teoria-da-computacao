from afd import DetFiniteAutomaton as DFA

states = ['q0', 'q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7']

automato = DFA(symbols='abc', states=states, start='q0', finals=['q3', 'q7'])
automato.add_transition('q0', 'a', 'q1')
automato.add_transition('q1', 'a', 'q2')
automato.add_transition('q2', 'a', 'q3')
automato.add_transition('q3', 'b', 'q3')
automato.add_transition('q3', 'c', 'q3')
automato.add_transition('q0', 'b', 'q4')
automato.add_transition('q0', 'c', 'q4')
automato.add_transition('q4', 'b', 'q4')
automato.add_transition('q4', 'c', 'q4')
automato.add_transition('q4', 'a', 'q5')
automato.add_transition('q5', 'a', 'q6')
automato.add_transition('q6', 'a', 'q7')

lista_teste = ['aaa', 'aaabbc', 'caaa', 'aaabaaa', 'caaac', 'aaabccb', 'bccbaaa', 'aaabccbaaa']

for palavra in lista_teste:
    accepted = f'ACEITA' if automato.accept(palavra) else 'NÃO ACEITA'
    print(f'{palavra:12} {accepted}')
