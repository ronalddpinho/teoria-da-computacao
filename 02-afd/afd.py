from typing import List, Set


class Transition:
    initial_state: str
    symbol: str
    target_state: str

    def __init__(self, initial: str, symbol: str, target: str):
        self.initial_state = initial
        self.symbol = symbol
        self.target_state = target


# Classe principal que define o autômato finito determinístico e suas
# propriedades, os conjuntos de símbolos, de estados e de estados finais são
# definidos pela estrutura de Set (conjunto).
class DetFiniteAutomaton:
    alphabet: Set[str]
    states: Set[str]
    initial_state: str
    final_states: Set[str]
    transitions: List[Transition]

    def __init__(self, symbols: str, states: List[str], start: str, finals: List[str]):       
        if start not in states:
            raise ValueError('Initial state is not in states set')

        self.initial_state = start
        self.states = set(states)
        self.alphabet = set(symbols)
        self.final_states = set(finals)
        self.transitions = []


    def add_transition(self, from_state: str, symbol: str, to_state: str) -> None:
        assert (from_state in self.states)
        assert (symbol in self.alphabet)
        assert (to_state in self.states)
        self.transitions.append(Transition(from_state, symbol, to_state))

    
    # Recebe o estado de partida e o símbolo e retorna o estado alvo
    def process(self, from_state: str, symbol: str) -> str:
        assert (from_state in self.states)
        assert (symbol in self.alphabet)

        for regra in self.transitions:
            if from_state == regra.initial_state and symbol == regra.symbol:
                return regra.target_state
        
        raise ValueError('Not possible process this transition')

    
    def accept(self, target_string: str, verbose = False) -> bool:
        for char in target_string:
            if char not in self.alphabet:
                raise ValueError(f'Symbol "{char}" is not in alphabet')

        char_sequence = []
        char_sequence[:0] = target_string
        current_state = self.initial_state
        
        try:
            while len(char_sequence) > 0:
                if verbose: print(f'Sequência: {char_sequence}')
                symbol = char_sequence.pop(0)
                if verbose: print(f'{current_state} processa {symbol} -> ', end='')
                current_state = self.process(current_state, symbol)
                if verbose: print(current_state)
        except ValueError as e:
            if verbose: print(e)
            return False

        if current_state in self.final_states:
            return True
        else: return False

