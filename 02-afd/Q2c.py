from afd import DetFiniteAutomaton as DFA

states = ['q0', 'q1', 'q2', 'q3', 'q4', 'q5']
final_states = ['q1', 'q2', 'q4', 'q5']

automato = DFA(symbols='ab', states=states, start='q0', finals=final_states)
automato.add_transition('q0', 'a', 'q1')
automato.add_transition('q0', 'b', 'q2')
automato.add_transition('q1', 'a', 'q3')
automato.add_transition('q1', 'b', 'q5')
automato.add_transition('q3', 'a', 'q3')
automato.add_transition('q3', 'b', 'q4')
automato.add_transition('q5', 'b', 'q5')

lista_teste = ['a', 'b', 'aab', 'abb', 'aabb', 'aa', 'bb']

for palavra in lista_teste:
    accepted = f'ACEITA' if automato.accept(palavra) else 'NÃO ACEITA'
    print(f'{palavra:12} {accepted}')