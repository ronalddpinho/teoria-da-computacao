# Trabalho de Expressões Regulares

> _Trabalho 1 de teoria da Computação, disciplina ministrada pelo professor
> Reginaldo pelo Programa de Pós-Graduação em Ciência da Computação (PPGCC) da
> UFPA._

### Como compilar

O código foi todo desenvolvido em um ambiente Unixe um Makefile é fornecido para
automatizar a compilação. É só usar o comando `make` em um terminal (no
diretório raíz desse projeto).

```
make
```

Um executável `runall` é gerado, então pra executar é só:

```
./runall
```

Para adicionar mais strings para testes é só adicionar mais elementos nos
vetores de strings no arquivo `src/main.cpp`, e então recompilar.