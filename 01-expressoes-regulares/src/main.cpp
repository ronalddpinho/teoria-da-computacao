#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
using std::vector;
using std::string;

#include "matchers.hpp"

#if __unix__
#   define ECHO_ACCEPTED "\e[38;5;48m[Accepted]\e[0m"
#   define ECHO_REJECTED "\e[38;5;9m[Rejected]\e[0m"
#else
#   define ECHO_ACCEPTED "[Accepted]"
#   define ECHO_REJECTED "[Rejected]"
#endif

vector<string> read_strings_from_file(std::string filename) {
    std::ifstream file(filename);
    std::vector<std::string> vec;
    std::string str;

    if (file.is_open()) {
        while (std::getline(file, str))
            vec.push_back(str);
        file.close();
    } else {
        throw std::runtime_error("Impossível abrir o arquivo");
    }
    return vec;
}

void display_result(RegexMask &rem, const std::string text)
{
    if (rem.Match(text)) {
        std::cout << ECHO_ACCEPTED << "  " << text << std::endl;
    } else {
        std::cout << ECHO_REJECTED << "  " << text << std::endl;
    }
}

int main(int argc, char **argv)
{
    //----------------------------------------------------
    // Lendo as strings de entrada dos arquivos de texto
    //----------------------------------------------------
    vector<string> names = read_strings_from_file("data/nomes.txt");
    vector<string> emails = read_strings_from_file("data/emails.txt");
    vector<string> cpfs = read_strings_from_file("data/cpf.txt");
    vector<string> numbers = read_strings_from_file("data/numeros.txt");
    vector<string> phones = read_strings_from_file("data/telefones.txt");
    vector<string> datetimes = read_strings_from_file("data/dataehora.txt");

    //----------------------------------------------------
    // Instanciação dos objetos de máscara
    //----------------------------------------------------

    NameMask name_mask;
    EmailMask email_mask;
    CpfMask cpf_mask;
    PhoneNumberMask phone_mask;
    DateTimeMask datetime_mask;
    RealNumberMask number_mask;

    // Uma função é criada para fazer a análise de todas as cadeias de um vector
    // Ele recebe o objeto de máscara (mask) e um vetor de strings e então
    // aplica essa máscara em todas as strings desse vector.
    auto&& check_all = [](RegexMask& mask, vector<string> strings) {
        for (string &s : strings) {
            display_result(mask, s);
        }    
    };

    //-----------------------------------------------------
    // Aplicando as máscaras nas strings de entrada
    //-----------------------------------------------------

    std::cout << "\n----- Nomes -----\n";
    check_all(name_mask, names);

    std::cout << "\n----- E-mails -----\n";
    check_all(email_mask, emails);

    std::cout << "\n----- CPFs -----\n";
    check_all(cpf_mask, cpfs);

    std::cout << "\n----- Telefones -----\n";
    check_all(phone_mask, phones);

    std::cout << "\n----- Data e Hora -----\n";
    check_all(datetime_mask, datetimes);

    std::cout << "\n----- Números Reais -----\n";
    check_all(number_mask, numbers);

    return EXIT_SUCCESS;
}