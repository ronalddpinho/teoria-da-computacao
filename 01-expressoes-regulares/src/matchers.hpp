#ifndef REGEX_MATCHER_HPP_
#define REGEX_MATCHER_HPP_

#include <regex>
#include <string>

// Class abstrata para base de todos os validadores propostos no trabalho
class RegexMask {
public:
    RegexMask(const char *regex_def) : re(regex_def) {}

    bool Match(std::string text) {
        return std::regex_match(text, this->re);
    }

protected:
    std::regex re;
};

// Classe para o padrão de Nome e Sobrenome 
// Valida um nome e sobre, ambos não vazios, separados por um espaço, não aceita
// caractres especiais ou numéricos, o primeiro símbolo do nome e do sobrenome
// deve ser maiúsculo, e os outros símbolos minúsculo.
class NameMask : public RegexMask {
public:
    NameMask() : RegexMask("^[A-Z][a-z]+\\s[A-Z][a-z]+$") {}
};

// Classe para máscara do padrão de e-mail.
// Valida sentenças que possuem símbolos minúsculos e contém exatamente um
// símbolo "@" que não esteja no início da sentença, e terminam com ".br" e
// devem ter ao menos um símbolo minúsculo entre o "@" e o ".br".
class EmailMask : public RegexMask {
public:
    EmailMask() : RegexMask("^[a-z]+@[a-z]+\\.br$") {}
};

// Classe para máscara de senhas
// Valida cadeias que podem conter símbolos minúsculos, maiúsculos e numéricos;
// devem, obrigatoriamente, ter pelo menos um símbolo do alfabeto maiúsculo e um
// símbolo do alfabeto numérico; devem ter comprimento igual a 8.
class PasswordMask : public RegexMask {
public:
    PasswordMask() : RegexMask("^$") {}
};

// TODO: Classe para máscara de CPF.
// Valida sentenças com o formato xxx.xxx.xxx-xx
// Com x sendo números.
class CpfMask : public RegexMask {
public:
    CpfMask() : RegexMask("^\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}$") {}
};

// TODO: Classe para máscara de Telefone.
// Valida sentenças com um dos formatos:
// - (xx) 9xxxx-xxxx
// - (xx) 9xxxxxxxx
// - xx 9xxxxxxxx
// com x sendo números.
class PhoneNumberMask : public RegexMask {
public:
    PhoneNumberMask() : RegexMask("^\\(\\d{2}\\)\\s9\\d{4}\\-?\\d{4}|\\d{2}\\s9\\d{8}$") {}
};

// TODO: Classe para máscara de data e hora.
// Valida sentenças com o formato dd/mm/aaaa hh:mm:ss
// Onde todas as letras são símbolos numéricos.
class DateTimeMask : public RegexMask {
public:
    DateTimeMask() : RegexMask("^\\d{2}/\\d{2}/\\d{4}\\s\\d{2}:\\d{2}:\\d{2}$") {}
};;

// TODO: Classe para máscar de número real com ou sem sinal.
// As sentenças devem começar com {+, -, <vazio>} em seguida deve ter pelo menos
// um número, depois um símbolo "." e então pelo menos um símbolo numérico.
class RealNumberMask : public RegexMask {
public:
    RealNumberMask() : RegexMask("^(\\+|\\-)?\\d+(\\.\\d+)?$") {}
};

#endif